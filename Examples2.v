Require Import Relations Morphisms Setoid.
Require Import Program.
Require Import Bool.
Require Memory Terms Axioms Booleans Lemmas.
Set Implicit Arguments.

Module Make(Import M: Memory.T).
  Module Export Examples2Exp := Lemmas.Make(M).
  
  Lemma decoration_test1: is (0,1) (while(tt unit, id)).
  Proof.
    decorate.
  Qed.

  Lemma decoration_test2: forall X (i: Loc), is (2,1) ((@forget X) o while(tt unit,id) o (lookup i) o (update i)).
    intros.
    decorate.
  Qed.
  
  Lemma while_proj1: forall X Y (b: term X B) (b': term (X*Y) B) (f: term X X), b' ==(tt (X*Y)) pi1 o b -> pi1 o while(b,f) ==(b') pi1 o (f o while(b,f)).
  Proof.
    intros.
    apply psubs with b.
    apply whileb_t.
    apply stow.
    assumption.
  Qed.
  
  Lemma while_proj2: forall X Y (b: term X B) (b': term (Y*X) B) (f: term X X), b' ==(tt (Y*X)) pi2 o b -> pi2 o while(b,f) ==(b') pi2 o (f o while(b,f)).
  Proof.
    intros.
    apply psubs with b.
    apply whileb_t.
    apply stow.
    assumption.
  Qed.
  
End Make.