Require Import Relations Morphisms.
Require Import Program.
Require Import Bool.
Require Memory Terms Axioms.
Set Implicit Arguments.

Module Make(Import M: Memory.T).
  Module Export BooleansExp := Axioms.Make(M).
  
  Ltac solve_bool := repeat(rewrite -> tb_and
                             || rewrite -> tb_or
                             || rewrite -> tb_impl
                             || rewrite -> tb_neg
                             || rewrite -> tpurepure
                             || rewrite -> tpurebool
                             || rewrite -> tboolpure
                             || rewrite -> tboolbool
                             || compute
                             || assumption
                             || reflexivity
                           ).                                
End Make.
