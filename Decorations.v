Require Import Relations Morphisms.
Require Import Program.
(* Require Import ZArith.
Open Scope Z_scope. *)
Require Memory Terms.
Set Implicit Arguments.

Module Make(Import M: Memory.T).
  Module Export DecorationsExp := Terms.Make(M). 

 Definition kind := prod nat nat.
 (*
 Fixpoint plus (x: kind) (y: kind) :=
  match x with
    |(x1,x2) => (match y with
                   | (y1,y2) => (x1+y1,x2+y2)
                 end)
  end.

 Infix "+++" := plus (at level 80).
  *)
 Inductive is: kind -> forall X Y, term X Y -> Prop :=
 | is_tpure: forall X Y (f: X -> Y), is (0,0) (@tpure X Y f)
 | is_tbool: forall X (b: X -> B), is (0,0) (tbool b)
 | is_tilde: forall k X (b: term X B), is k b -> is k (tilde b)
 | is_comp: forall k X Y Z (f: term X Y) (g: term Y Z), is k f -> is k g -> is k (f o g)
 | is_pair: forall k X Y Z (f: term X Y) (g: term X Z), is k f -> is k g -> is k (pair f g)
 | is_lookup: forall i, is (1,0) (lookup i)   
 | is_update: forall i, is (2,0) (update i)
 | is_while0: forall X (b: term X B) (f: term X X) (k1 k2: nat), is (k1,0) f ->  is (k1,1) (while(b,f))
 | is_while1: forall X (b: term X B) (f: term X X) (k1 k2: nat), is (k1,1) f ->  is (k1,1) (while(b,f))
 | is_hierarchy00_r: forall X Y (f: term X Y), is (0,0) f -> is (0,1) f
 | is_hierarchy00_l: forall X Y (f: term X Y), is (0,0) f -> is (1,0) f
 | is_hierarchy01_l: forall X Y (f: term X Y), is (0,1) f -> is (1,1) f
 | is_hierarchy10_r: forall X Y (f: term X Y), is (1,0) f -> is (1,1) f
 | is_hierarchy10_l: forall X Y (f: term X Y), is (1,0) f -> is (2,0) f
 | is_hierarchy11_l: forall X Y (f: term X Y), is (1,1) f -> is (2,1) f
 | is_hierarchy20_r: forall X Y (f: term X Y), is (2,0) f -> is (2,1) f.
 

(*---*)

 Lemma is_id X: is (0,0) (@id X).
 Proof. apply is_tpure. Qed.
 
 Lemma is_pi1 X Y: is (0,0) (@pi1 X Y).
 Proof. apply is_tpure. Qed.
 
 Lemma is_pi2 X Y: is (0,0) (@pi2 X Y).
 Proof. apply is_tpure. Qed.
 
 Lemma is_forget X: is (0,0) (@forget X).
 Proof. apply is_tpure. Qed.
 
 Lemma is_constant (i: Loc) (v: Val i): is (0,0) (@constant i v).
 Proof. apply is_tpure. Qed.
 
 Lemma is_tt X: is (0,0) (tt X).
 Proof.
   unfold tt.
   apply is_comp.
   apply is_forget.
   apply is_tbool.
 Qed.
 
Lemma is_ff X: is (0,0) (ff X).
 Proof.
   unfold ff.
   apply is_comp.
   apply is_forget.
   apply is_tbool.
 Qed.
 
(*---*)

 Hint Constructors is.

 Ltac decorate :=  solve[
                       assumption |
                       constructor; decorate |
                       apply is_comp; decorate |
                       apply is_pair; decorate |
                       apply is_tilde; decorate |
                       apply is_id; decorate |
                       apply is_forget; decorate |
                       apply is_pi1; decorate |
                       apply is_pi2; decorate |
                       apply is_constant; decorate |
                       apply is_while0; decorate |
                       apply is_while1; decorate |
                       apply is_lookup; decorate |
                       apply is_update; decorate |
                       apply is_tbool; decorate |
                       apply is_tpure; decorate |
                       apply is_tt; decorate |
                       apply is_ff; decorate |
		       apply is_hierarchy00_r; decorate |
                       apply is_hierarchy00_l; decorate |
                       apply is_hierarchy01_l; decorate |
                       apply is_hierarchy10_r; decorate |
                       apply is_hierarchy10_l; decorate |
                       apply is_hierarchy11_l; decorate |
                       apply is_hierarchy20_r; decorate
                     ].



 
(* TODO: Fix if used, delete if not.
 Class PURE_ST {A B: Type} (f: term A B) := isp : is pure f.
 Hint Extern 0 (PURE _) => decorate : typeclass_instances.

 Class RO_ST {A B: Type} (f: term A B) := isro : is ro f.
 Hint Extern 0 (RO _) => decorate : typeclass_instances.

 Class RW_ST {A B: Type} (f: term A B) := isrw : is rw f.
 Hint Extern 0 (RW _) => decorate : typeclass_instances.
*)

End Make.
