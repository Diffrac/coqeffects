Require Import Relations Morphisms.
Require Import Program.
Require Import Le Gt Minus Bool Setoid.
Set Implicit Arguments.

Module Type T.
  Parameter B: Type.
  Parameter Loc: Type.
  Parameter Val: Loc -> Type.
  Parameter Loc_dec: forall i j: Loc, {i=j}+{i<>j}.
End T.
