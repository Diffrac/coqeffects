Require Import Relations Morphisms Setoid.
Require Import Program.
Require Import Bool.
Require Import ZArith.
Open Scope Z_scope.
Require Memory Terms Axioms Booleans Lemmas.
Set Implicit Arguments.

Module Make(Import M: Memory.T).
  Module Export ExamplesExp := Lemmas.Make(M).      


  Lemma bool_simplification1: neg(or (@tbool unit (fun _ => true)) (@tbool unit (fun _ => false))) ==(tt unit) @tbool unit (fun _ => false).
  Proof.
    intros.
    try solve_bool.
  Qed.
  
  Lemma bool_simplification2: and
                     (or
                        (or (@tbool unit (fun _ => true)) (@tbool unit(fun _ => false)))
                        (@tbool unit(fun _ => false))
                     )
                     (@tbool unit(fun _ => true))
                    ==(tt unit) @tbool unit(fun _ => true).
  Proof.
    try solve_bool.
  Qed.
    
  Lemma ifb_idempot: forall (X: Type) (b b': term X B) (Y: Type) (f g: term X Y),
                       tilde(b') o choose(
                              tilde(b') o choose(f,g),
                              g)
                       ==(tt X) tilde(b') o choose(f,g).
  Proof.
    intros.
    apply case_b with b'.
    apply choice_made_t.
    transitivity g.
    apply choice_made_f.
    symmetry.
    apply choice_made_f. 
  Qed.

  Lemma ifb_contradic_hypoth: forall (X: Type) (b b': term X B) (Y: Type) (f g: term X Y),
                        tilde(b') o choose(
                               tilde(neg(b')) o choose(f,g),
                               g)
                        ==(tt X) g.
    intros.
    apply case_b with b'.
    transitivity (tilde(neg b') o choose(f,g)).
    apply choice_made_t.
    apply same_bool_same_eq with (neg(neg b')).
    repeat constructor.
    apply negneg.
    apply choice_made_f.
    apply choice_made_f.    
  Qed.
  
  
  Lemma absv_pos: tilde(tbool(fun (x: Z) => (x >=? 0): bool)) o choose(@id Z, tpure(fun (x: Z) => (-x: Z))) o tbool(fun (x: Z) => (x >=? 0): bool) ==(tt Z) tt Z.
  Proof. 
    apply case_b with (tbool(fun (x: Z) => (x >=? 0): bool)).
    (* begin hide *)
    (* Case x >=? 0 *)
    (* end hide *)
    transitivity (id o tbool(fun x: Z => x >=?0)).
    apply cont.
    rewrite ids.
    apply ifself.
    (* begin hide *)
    (* Case negb(x >=? 0) *)
    (* end hide *)
    transitivity (id o tbool(fun x: Z => negb(x >=?0))).
    transitivity (tpure(fun x: Z => -x) o (tbool(fun x: Z => x >=? 0))).
    apply cont.
    rewrite ids.
    apply same_bool_same_eq with (tbool(fun x: Z => negb(x >=? 0))).
    repeat constructor.
    symmetry.
    apply tb_neg.
    rewrite tpurebool.
    (* begin hide *)
    (* The current subgoal means "if neg(x>=0), then (neg(x>=0) if and only if -x >=0)",
       which is true, but I didn't manage to prove it. This subgoal being
       simple, we admit it to move forward. *)
    (* end hide *)
    admit.
    rewrite ids.
    apply same_bool_same_eq with (tbool(fun x: Z => negb(x >=?0))).
    repeat constructor.
    symmetry.
    apply tb_neg.
    apply ifself.
  Qed.

End Make.