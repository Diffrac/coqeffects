Require Import Relations Morphisms Setoid.
Require Import Program.
Require Import Bool.
Require Memory Terms Decorations.
Set Implicit Arguments.

Module Make(Import M: Memory.T).
  Module Export AxiomsExp := Decorations.Make(M).

 Reserved Notation "x ==( b ) y" (at level 80).
 Reserved Notation "x ~( b ) y" (at level 80).

  (* comp_snd is only used as a tool in the definition of the axioms, nothing more *)
 Definition comp_snd {X Y Z} (g : term Y Z) := (fun (f : term X Y) => comp f g).

 
 Definition idem (X: Type) (b: term X B) (Y: Type)(f1 f2: X -> Y) := f1 = f2.
 Definition idem_term (X: Type) (b: term X B) (Y: Type) (f1 f2: term X Y) := f1 = f2.
 
 (* ############ Begin axiom definitions ############ *)

 Inductive strong_b: forall (X: Type) (b: term X B) (Y: Type), relation (term X Y) :=


 (* ################# *)
 (* Categorical rules *)
 (* ################# *)                   

 (* id as source *)
 | ids: forall (X: Type) (b: term X B) Y (f: term X Y), (id o f) ==(b) f
 (* id as target *)
 | idt: forall (X: Type) (b: term X B) Y (f: term X Y), (f o id) ==(b) f
 | assoc: forall (X: Type) (b: term X B) Y Z T (f: term X Y) (g: term Y Z) (h: term Z T), f o (g o h) ==(b) (f o g) o h

                                                                                                            
 (* ################ *)
 (* Congruence rules *)
 (* ################ *)

 | sb_refl: forall X b Y, Reflexive (@strong_b X b Y)
 | sb_sym: forall X b Y, Symmetric (@strong_b X b Y)
 | sb_trans: forall X b Y, Transitive (@strong_b X b Y)
 (* ==(b) always satisfies continuation *)
 | cont: forall (X Y Z: Type) (b: term X B) (g1 g2: term X Y) (h: term Y Z), g1 o h ==(b) g2 o h
 (* ==(b) verifies a weak form of substitution *)
 | psubs: forall (X Y Z: Type) (b: term Y B) (b': term X B) (f: term X Y) (g1 g2: term Y Z),
            g1 ==(b) g2 -> b' ~(tt X) f o b -> f o g1 ==(b') f o g2
 (* ==(b) does not verify substitution in general, but ==(tt) is the same
    relation as ==, the strong equality, and therefore does verify it *)
 | cont_subs_t: forall X Y Z, Proper (@strong_b X (tt X) Y ==> @strong_b Y (tt Y) Z ==> @strong_b X (tt X) Z) comp
                                                                  
 (* The following are compatibility rules *)
 | and_comp: forall (X: Type) (b: term X B) (f1 f2 g1 g2: term X B), f1 ==(b) f2 -> g1 ==(b) g2 -> and f1 g1 ==(b) and f2 g2
 | or_comp: forall (X: Type) (b: term X B) (f1 f2 g1 g2: term X B), f1 ==(b) f2 -> g1 ==(b) g2 -> or f1 g1 ==(b) or f2 g2
 | impl_comp: forall (X: Type) (b: term X B) (f1 f2 g1 g2: term X B), f1 ==(b) f2 -> g1 ==(b) g2 -> impl f1 g1 ==(b) impl f2 g2
 | neg_comp: forall (X: Type) (b: term X B) (f g: term X B), f ==(b) g -> neg f ==(b) neg g
 | tilde_comp: forall (X: Type) (b: term X B) (f g: term X B), f ==(b) g -> tilde f ==(b) tilde g

 (* ################################ *)
 (* Hierarchy rules : weak to strong *)
 (* ################################ *)

 | wtos: forall X Y (b: term X B) (f g: term X Y) (k2: nat), is (1,k2) f -> is (1,k2) g -> f ~(b) g -> f ==(b) g
 | wtos_loc: forall X (b: term X B) (f g: term X unit), (forall i: Loc, f o (lookup i) ~(b) g o (lookup i)) -> f ==(b) g

 (* ############# *)
 (* Miscellaneous *)
 (* ############# *)

 (* tpure preserves the pure composition (morphism property of tpure) *)
 (* NB : compose, the Coq composition function, does not follow our convention ; we note 'compose g f' to denote 'f o g' *)
 | tcomp: forall X Y Z (b: term X B) (f: X -> Y) (g: Y -> Z), tpure (compose g f) ==(b) (tpure f) o (tpure g)
                                                                                        
 (* A rule for pairs *)
 | pair2: forall X Y Y' (b: term X B) (f1: term X Y) (f2: term X Y') (k2: nat), is (1,k2) f1 -> (pair f1 f2) o pi2 ==(b) f2

 (* A decomposition of strong equivalence as weak equivalence + a "state only" evaluation *)
 | wtos_forget: forall X Y (b: term X B) (f g: term X Y), f o forget ==(b) g o forget -> f ~(b) g -> f ==(b) g

 (* ############### *)
 (* Non termination *)
 (* ############### *)

 | unfold_while: forall X (b b': term X B) (f: term X X), while(b',f) ==(b) tilde(b') o choose(f o while(b',f),id)
 | weak_induction_axiom: forall X (b b1 b2: term X B) (f1 f2: term X X), b1 ==(tt X) b2 -> f1 ==(b) f2 -> while(b1,f1) ==(b) while(b2,f2)
                                                                          
 (* ############## *)
 (* Term unicities *)                                                         
 (* ############## *)

 | forg_unic: forall (X: Type) (b: term X B) (f: term X unit), f ==(b) @forget X
 | choose_unic: forall (X Y: Type) (f1 f2: term X Y) (g: term (X+X) Y), (tilde (tt X)) o g ==(tt X) f1 -> (tilde (ff X)) o g ==(tt X) f2 -> g ==(tt (X+X)) choose(f1,f2)
 | while_unic: forall X (b b': term X B) (f g: term X X), g ==(b) f o g -> g ==(neg b) id -> g ==(tt X) while(b,f)


 (* ######################### *)                                                          
 (* Direct boolean operations *)
 (* ######################### *)

 | tb_and: forall (X: Type) (b: term X B) (b1 b2: X -> bool), and (tbool b1) (tbool b2) ==(b) tbool (fun x => andb (b1 x) (b2 x))
 | tb_or: forall (X: Type) (b: term X B) (b1 b2: X -> bool), or (tbool b1) (tbool b2) ==(b) tbool (fun x => orb (b1 x) (b2 x))
 | tb_impl: forall (X: Type) (b: term X B) (b1 b2: X -> bool), impl (tbool b1) (tbool b2) ==(b) tbool (fun x => implb (b1 x) (b2 x))
 | tb_neg: forall (X: Type) (b: term X B) (b': X -> bool), neg (tbool b') ==(b) tbool (fun x => negb (b' x))                                                                        

 (* #################### *)
 (* Boolean manipulation *)
 (* #################### *)

 | ifself: forall (X: Type) (b: term X B), b ==(b) tt X
 | implic: forall (X: Type) (b: term X B) (b': term X B), impl b b' ==(b) tt X -> b' ==(b) tt X
 (* Choice reduction *)
 | choose_t: forall (X Y: Type) (b: term X B) (f1 f2: term X Y), tilde(tt X) o choose(f1,f2) ==(b) f1
 | choose_f: forall (X Y: Type) (b: term X B) (f1 f2: term X Y), tilde(ff X) o choose(f1,f2) ==(b) f2
 | negneg: forall (X: Type) (b: term X B) (b': term X B), neg (neg b') ==(b) b'
 | negtf: forall (X: Type) (b: term X B), neg(tt X) ==(b) ff X
 | case_b: forall (X: Type) (b: term X B) (Y: Type)  (f g: term X Y), f ==(b) g -> f ==(neg b) g -> f ==(tt X) g
 | same_bool_same_eq: forall (X Y: Type) (b: term X B) (b1 b2: term X B) (f g: term X Y),
                        b1 ==(tt X) b2 -> f ==(b1) g -> f ==(b2) g

 (* ############################################## *)
 (* tpure and tbool constructed terms manipulation *)
 (* ############################################## *)
 | tpurepure: forall (X Y Z: Type) (b: term X B) (f: X -> Y) (g: Y -> Z), tpure(f) o tpure(g) ==(b) tpure(fun x => g(f(x)))
 | tpurebool: forall (X Y: Type) (b: term X B) (b': Y -> bool) (f: X -> Y),
                tpure(f) o tbool(b') ==(b) tbool(fun x => b'(f(x)))
 | tboolpure: forall (X Y: Type) (b: term X B) (b': X -> B) (f: B -> Y), tbool(b') o tpure(f) ==(b) tpure(fun x => f(b'(x)))
 | tboolbool: forall (X: Type) (b: term X B) (b1: X -> B) (b2: B -> B), tbool(b1) o tbool(b2) ==(b) tbool(fun x => b2(b1(x)))


 with weak_b: forall (X: Type) (b: term X B) (Y: Type), relation (term X Y) :=
   
 (* ############################## *)
 (* ### Weak equivalence rules ### *)
 (* ############################## *)
   
 (* ################ *)
 (* Congruence rules *)
 (* ################ *)
   
 (* Weak equivalence symmetry *)
 | wb_sym: forall X (b: term X B) Y, Symmetric(@weak_b X b Y)
 (* Weak equivalence transitivity *)
 | wb_trans: forall X (b: term X B) Y, Transitive(@weak_b X b Y)
                                              
 (* NB : The following two are the composition rules, inverted for a monad *)
 (* Partial continuation *) (* WARNING: subtle! TODO *)
 | pwcont: forall X Y Z (b: term X B) (f:term Y Z) (k2: nat), (is (0,k2) f) -> Proper(@weak_b X b Y ==> @weak_b X b Z) (comp_snd f)
 (* Substitution *) (* WARNING: subtle! TODO *)

 (* ~(tt X) verifies total substitution *)
 | wsubs: forall X Y Z, Proper(@idem_term X (tt X) Y ==> @weak_b Y (tt Y) Z ==> @weak_b X (tt X) Z) (comp)  
                                                                                    
 (* ~(b) verifies a weak form of substitution, similarly to ==(b) *)
 | pwsubs: forall (X Y Z: Type) (b: term Y B) (b': term X B) (f: term X Y) (g1 g2: term Y Z),
             g1 ~(b) g2 -> b' ~(tt X) f o b -> f o g1 ~(b') f o g2

 (* The following are compatibility rules *)
 | w_and_comp: forall (X: Type) (b: term X B) (f1 f2 g1 g2: term X B), f1 ~(b) f2 -> g1 ~(b) g2 -> and f1 g1 ~(b) and f2 g2
 | w_or_comp: forall (X: Type) (b: term X B) (f1 f2 g1 g2: term X B), f1 ~(b) f2 -> g1 ~(b) g2 -> or f1 g1 ~(b) or f2 g2
 | w_impl_comp: forall (X: Type) (b: term X B) (f1 f2 g1 g2: term X B), f1 ~(b) f2 -> g1 ~(b) g2 -> impl f1 g1 ~(b) impl f2 g2
 | w_neg_comp: forall (X: Type) (b: term X B) (f g: term X B), f ~(b) g -> neg f ~(b) neg g
 | w_tilde_comp: forall (X: Type) (b: term X B) (f g: term X B), f ~(b) g -> tilde f ~(b) tilde g

                             
 (* Hierarchy rule : strong to weak *)
 | stow: forall X Y (b: term X B) (f g:term X Y), f ==(b) g -> f ~(b) g

                                                                 
 (* ############### *)
 (* Non termination *)                                           
 (* ############### *)
                                                               
 | w_weak_induction_axiom: forall X (b b1 b2: term X B) (f1 f2: term X X), b1 ~(tt X) b2 -> f1 ~(b) f2 -> while(b1,f1) ~(b) while(b2,f2)
                                                                 
 (* ############# *)
 (* Miscellaneous *)
 (* ############# *)

 (* Rules for pairs *)                                              
 | pair1: forall X Y Y' (b: term X B) (f1: term X Y) (f2: term X Y') (k2: nat), (is (1,k2) f1) -> (pair f1 f2) o pi1 ~(b) f1
 | pair3: forall X Y Y' (b: term X B) (f g: term X (Y*Y')), f o pi1 ~(b) g o pi1 -> f o pi2 ~(b) g o pi2 -> f ~(b) g

 (* Update-lookup interactions *)                                                                                        
 | up_look_eq: forall i (b: term (Val i) B), (update i) o (lookup i) ~(b) id
 | up_look_neq2: forall i j (b: term (Val i) B), i<>j -> update i o lookup j ~(b) forget o lookup j

 (* Unicity of the type X -> () term ('forget') up to weak equivalence *)
 | unitw: forall X (b: term X B) (f g: term X unit), f ~(b) g

 where "x ==( b ) y" := (strong_b b x y)
                          and "x ~( b ) y":= (weak_b b x y).

 Notation "x == y" := (strong_b x y) (at level 70).
 Notation "x ~ y" := (weak_b x y) (at level 70).



 
 
(* The following will allow us to use the 'rewrite' tactic inside ==(b)
   equalites *)
 Add Parametric Relation {X: Type} (b: term X B) (Y: Type): (term X Y) (@strong_b X b Y)
    reflexivity proved by (@sb_refl X b Y)
    symmetry proved by (@sb_sym X b Y)
    transitivity proved by (@sb_trans X b Y)
    as sb_rel.                                                                       
 
 Add Parametric Morphism X (b: term X B) : (@and X) with
     signature (@strong_b X b B) ==> (@strong_b X b B) ==> (@strong_b X b B) as and_rw.
   intros.
   apply and_comp.
   assumption.
   assumption.
 Qed.
 
 
 Add Parametric Morphism X (b: term X B) : (@or X) with
     signature (@strong_b X b B) ==> (@strong_b X b B) ==> (@strong_b X b B) as or_rw.
   intros.
   apply or_comp.
   assumption.
   assumption.
 Qed.
 
 
 Add Parametric Morphism X (b: term X B) : (@impl X) with
     signature (@strong_b X b B) ==> (@strong_b X b B) ==> (@strong_b X b B) as impl_rw.
   intros.
   apply impl_comp.
   assumption.
   assumption.
 Qed.
 
 
 Add Parametric Morphism X (b: term X B) : (@neg X) with
     signature (@strong_b X b B) ==> (@strong_b X b B) as neg_rw.
   intros.
   apply neg_comp.
   assumption.
 Qed.
 
 Add Parametric Morphism X (b: term X B) : (@tbool X) with
     signature (@idem X b B) ==> (@strong_b X b B) as tbool_rw.
   intros.
   rewrite H.
   reflexivity.
 Qed.
 
 Add Parametric Morphism X (Y: Type) (b: term X B) : (@tpure X Y) with
     signature (@idem X b Y) ==> (@strong_b X b Y) as tpure_rw.
   intros.
   rewrite H.
   reflexivity.
 Qed.
 
 Add Parametric Morphism X (b: term X B) : (tilde) with
     signature (@strong_b X b B) ==> (@strong_b X b (X+X)) as tilde_rw.
   intros.
   apply tilde_comp.
   assumption.
 Qed.
 
 
End Make.