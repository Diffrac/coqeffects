Require Import Relations Morphisms Setoid.
Require Import Program.
Require Import Bool.
Require Memory Terms Axioms Booleans.
Set Implicit Arguments.

Module Make(Import M: Memory.T).
  Module Export LemmasExp := Booleans.Make(M).

 Lemma negneg_eqn: forall (X: Type) (b: term X B) (f g: term X B), f ==(b) g <-> neg(neg(f)) ==(b) neg(neg(g)).
 Proof.
   intros.
   split.
   intro.
   transitivity f.
   apply negneg.
   transitivity g.
   assumption.
   symmetry.
   apply negneg.
   intro.
   transitivity (neg(neg f)).
   symmetry.
   apply negneg.
   transitivity (neg(neg g)).
   assumption.
   apply negneg.
 Qed.

  Lemma neg_ft: forall (X: Type) (b: term X B), neg(ff X) ==(b) tt X.
  Proof.
    intros.
    rewrite negneg_eqn.
    apply neg_comp.
    transitivity (ff X).
    apply negneg.
    symmetry.
    apply negtf.
  Qed. 

  Lemma choice_made_t: forall (X Y: Type) (b: term X B) (f1 f2: term X Y), tilde(b) o choose(f1,f2) ==(b) f1.
  Proof.
    intros.
    transitivity (tilde(tt X) o choose(f1,f2)).
    apply cont.
    apply choose_t.
  Qed.
  
  Lemma choice_made_f: forall (X Y: Type) (b: term X B) (f1 f2: term X Y), tilde(b) o choose(f1,f2) ==(neg b) f2.
  Proof.
    intros.
    transitivity (tilde(ff X) o choose(f1,f2)).
    apply cont.  
    apply choose_f.
  Qed.

  Lemma whileb_t: forall X (b: term X B) (f: term X X), while(b,f) ==(b) f o while(b,f).
  Proof.
    intros.
    transitivity (tilde(b) o choose(f o while(b,f),id)).
    apply unfold_while.
    apply choice_made_t.
  Qed.

  Lemma whileb_f: forall X (b: term X B) (f: term X X), while(b,f) ==(neg b) id.
  Proof.
    intros.
    transitivity (tilde(b) o choose(f o while(b,f),id)).
    apply unfold_while.
    apply choice_made_f.
  Qed.

  Lemma wb_refl X b Y: Reflexive(@weak_b X b Y).
  Proof.
    constructor.
    apply stow.
    reflexivity.
  Qed.

  
  Add Parametric Relation {X: Type} (b: term X B) (Y: Type): (term X Y) (@weak_b X b Y)
      reflexivity proved by (@wb_refl X b Y)
      symmetry proved by (@wb_sym X b Y)
      transitivity proved by (@wb_trans X b Y)
        as wb_rel.
  
  
  (* A restricted substitution and continuation property for weak equivalence *)
  Lemma wsc: forall X Y Z, Proper (@strong_b X (tt X) Y ==> @weak_b Y (tt Y) Z ==> @weak_b X(tt X) Z) (comp).
  Proof.
    constructor.
    transitivity (x o y0).
    apply stow.
    apply cont_subs_t.
    symmetry.
    auto.
    reflexivity.
    apply wsubs.
    unfold idem_term.
    reflexivity.
    symmetry.
    assumption.
  Qed.     
  
  Instance s_sub_w: forall X b Y, subrelation (@strong_b X b Y) (@weak_b X b Y).
  Proof. constructor. apply stow. symmetry. assumption. Qed. 
  
  (* The following will allow us to use the rewrite tactic inside ~(b) equations *)



 Add Parametric Morphism X (b: term X B) : (@and X) with
     signature (@weak_b X b B) ==> (@weak_b X b B) ==> (@weak_b X b B) as and_rw.
   intros.
   apply w_and_comp.
   assumption.
   assumption.
 Qed.
 
 
 Add Parametric Morphism X (b: term X B) : (@or X) with
     signature (@weak_b X b B) ==> (@weak_b X b B) ==> (@weak_b X b B) as or_rw.
   intros.
   apply w_or_comp.
   assumption.
   assumption.
 Qed.
 
 
 Add Parametric Morphism X (b: term X B) : (@impl X) with
     signature (@weak_b X b B) ==> (@weak_b X b B) ==> (@weak_b X b B) as impl_rw.
   intros.
   apply w_impl_comp.
   assumption.
   assumption.
 Qed.
 
 
 Add Parametric Morphism X (b: term X B) : (@neg X) with
     signature (@weak_b X b B) ==> (@weak_b X b B) as neg_rw.
   intros.
   apply w_neg_comp.
   assumption.
 Qed.
 
 Add Parametric Morphism X (b: term X B) : (@tbool X) with
     signature (@idem X b B) ==> (@weak_b X b B) as tbool_rw.
   intros.
   rewrite H.
   reflexivity.
 Qed.
 
 Add Parametric Morphism X (Y: Type) (b: term X B) : (@tpure X Y) with
     signature (@idem X b Y) ==> (@weak_b X b Y) as tpure_rw.
   intros.
   rewrite H.
   reflexivity.
 Qed.
 
 Add Parametric Morphism X (b: term X B) : (tilde) with
     signature (@weak_b X b B) ==> (@weak_b X b (X+X)) as tilde_rw.
   intros.
   apply w_tilde_comp.
   assumption.
 Qed.

  (*
  Existing Instance cont_subs.
  Existing Instance pwcont.
  Existing Instance wsubs.
   *) (* TODO: What is this? *)
End Make.
