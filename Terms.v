Require Import Relations Morphisms.
Require Import Program Arith.
Require Memory.
Set Implicit Arguments.

Module Make(Import M: Memory.T).

  Definition B := bool: Type.                                            
  
  Inductive term: Type -> Type -> Type :=
  | comp:   forall {X Y Z: Type}, term X Y -> term Y Z -> term X Z
  (* Composition is, like in the paper, reversed *)
  | choose: forall {X Y: Type}, ((term X Y) * (term X Y)) -> term (X + X) Y
  | tpure:  forall {X Y: Type}, (X -> Y) -> term X Y
  | tbool: forall {X: Type}, (X -> bool) -> (term X B)
  (* tbool constructs a boolean in our framework from a Coq boolean *)
  | and: forall {X: Type}, (term X B) -> (term X B) -> (term X B)
  | or: forall {X: Type}, (term X B) -> (term X B) -> (term X B)
  | impl: forall {X: Type}, (term X B) -> (term X B) -> (term X B)
  | neg: forall {X: Type}, (term X B) -> (term X B)
  | tilde: forall {X: Type}, term X B -> term X (X+X)
  (* tilde is to be used only on terms that are booleans *)
  | pair:   forall {X Y Z: Type}, term X Y -> term X Z -> term X (Y*Z)
  | lookup: forall i:Loc, term unit (Val i)    
  | update: forall i:Loc, term (Val i) unit
  | while: forall {X: Type}, ((term X B) * (term X X)) -> term X X.
  (* loop(b,f) corresponds to while b do f *)

 Infix "o" := comp (at level 70).

 Definition id  {X: Type}     : term X X      := tpure id.
 Definition pi1 {X Y: Type}   : term (X*Y) X  := tpure fst. 
 Definition pi2 {X Y: Type}   : term (X*Y) Y  := tpure snd.
 Definition forget {X}        : term X unit   := tpure (fun _ => tt).
 Definition constant {i: Loc} (v: Val i): term unit (Val i)  := tpure (fun _ => v).
 Definition is_equal X (vi: nat) (vj: nat) := (@forget X) o (tbool(fun _ => beq_nat vi vj)).
 Definition minus_one (v: nat) := tpure(fun (x: nat) => x-1).
 (* tpure is a constructor that sends Coq functions to terms in our decorated environment *)
 
  Definition tt X := forget o (@tbool unit (fun _ => true)) : term X B.
  Definition ff X := forget o (@tbool unit (fun _ => false)) : term X B.
 
End Make.